{{ HTML::style('public/css/catalogo.css') }}
@extends('templates.default')
@section('content')
    <div class="container">
        <div class="row" style=" padding-top: 5%; padding-bottom: 5%">
        	<div class="col-md-6">
        		<div class="row" ">
        			<div class="col-md-6"><img style="margin-top: 25px;" class=" img-fluid slide-produto animated fadeIn w-100" src="{{URL('public/img/catalogo/00.jpg')}}" >  </div>
        			<div class="col-md-6"><img style="margin-top: 25px;" class=" img-fluid slide-produto animated fadeIn w-100" src="{{URL('public/img/catalogo/06.jpg')}}" >  </div>


        		</div>
        		<div class="row">
        			<div class="col-md-6"><img style="margin-top: 25px;" class=" img-fluid slide-produto animated fadeIn w-100" src="{{URL('public/img/catalogo/04.jpg')}}" >  </div>
        			<div class="col-md-6"><img style="margin-top: 25px;" class=" img-fluid slide-produto animated fadeIn w-100" src="{{URL('public/img/catalogo/05.jpg')}}" >  </div>
        			
        		</div>

        	</div>
        	<div class="col-md-6">
        		<div id="carouselExampleControls" style="margin-top: 25px;"  class="carousel slide" data-ride="carousel">
	                <div class="carousel-inner">               	
	                    <div class="carousel-item active">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/01.jpg')}}" alt="First slide">                            
	                    </div> 
	                    <div class="carousel-item">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/02.jpg')}}" alt="First slide">                            
	                    </div>
	                    <div class="carousel-item">                        
	                       	<img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/03.jpg')}}" alt="First slide">                            
	                    </div>                                            
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Previous</span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Next</span>
	                </a>
            	</div>            
        	</div>
        	<div class="col-md-6">
        		<div id="carouselExampleControls3" style="margin-top: 25px;"  class="carousel slide" data-ride="carousel">
	                <div class="carousel-inner">               	
	                    <div class="carousel-item active">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/09.jpg')}}" alt="First slide">                            
	                    </div> 
	                    <div class="carousel-item">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/10.jpg')}}" alt="First slide">                            
	                    </div>                                          
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls3" role="button" data-slide="prev">
	                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Previous</span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-slide="next">
	                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Next</span>
	                </a>
            	</div> 
        	</div>
        	<div class="col-md-6">
        		<div id="carouselExampleControls2" style="margin-top: 25px;"  class="carousel slide" data-ride="carousel">
	                <div class="carousel-inner">               	
	                    <div class="carousel-item active">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/07.jpg')}}" alt="First slide">                            
	                    </div> 
	                    <div class="carousel-item">                        
	                        <img class=" img-fluid w-100 slide-produto animated fadeIn" src="{{URL('public/img/catalogo/08.jpg')}}" alt="First slide">                            
	                    </div>                                          
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
	                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Previous</span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
	                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	                    <span class="sr-only">Next</span>
	                </a>
            	</div> 
        	</div>
        	<div class="col-md-3"></div>
        	<div class="col-md-3"></div>
        	<div class="col-md-3"></div>
       	</div>
     </div>
@endsection