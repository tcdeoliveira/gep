{{ HTML::style('public/css/quemsomos.css') }}
@extends('templates.default')
@section('content')
    <div class="container div-sobre">
        <div class="row   " style=" min-height: 300px; padding-top: 5%; padding-bottom: 5%" >
            <div class="col-md-12  text-center">
                <h3 class="animated fadeIn"  >Quem Somos?</h3>
                <p class="animated fadeIn"style="text-align: justify-all;">
                    Fundada na cidade de Fortaleza, a GEP é uma empresa familiar genuinamente cearense, representando indústrias  conceituadas no segmento de  produtos alimentícios e atuando na  distribuição de alimentos em sachês.
                </p>
                <hr>
            </div>
            <div class="col-md-4 "style="text-align: justify-all;">
                <h3 class="animated fadeIn fadeIn"  >Missão</h3>
                <p class="animated fadeIn" style="text-align: justify-all;" >
                     Representar indústrias alimentícias oferecendo produtos com alto padrão de qualidade, distribuir alimentos em Sachê proporcionando praticidade e economia, buscando a satisfação de seus consumidores.
                </p>
                <hr>
            </div>
            <div class="col-md-4 "style="text-align: justify-all;">
                <h3 class="animated fadeIn"  >Visão </h3>
                <p class="animated fadeIn"style="text-align: justify-all;" >
                    Ser reconhecida como uma empresa conceito no setor de representação comercial de produtos alimentícios e tornar-se líder no segmento de distribuição de alimentos em Sachê.
                </p>
                <hr>
                
            </div>
            <div class="col-md-4 "style="text-align: justify-all;">                
                
                    <h3 class="animated fadeIn"  >Quem é o nosso público alvo?</h3>
                    <p class="animated fadeIn" style="text-align: justify-all;">
                        Atacado, Varejo, Food Service, Hotéis e restaurantes.
                    </p>
                <hr>
            </div>                  
        </div>
    </div>    
@endsection