{{ HTML::style('public/css/home.css') }}
@extends('templates.default')
@section('content')
    <div class="container-fluid h-100 div-one">
        <div class="row  align-items-center h-100 mx-auto  "  >
            <div id="carouselExampleControls"   class="carousel slide col-md-12" data-ride="carousel">
                <div class="carousel-inner">
                    
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-12 align-items-center text-center">
                                <img style="max-height: 350px !important; animation-delay: 0.7s" class=" img-fluid slide-produto animated fadeIn" src="{{URL('public/img/slide/sal-rosa.png')}}" alt="First slide">
                            </div>
                        </div>
                    </div>    
                    
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-8 offset-md-2 text-center">
                                <h2 class="animated fadeIn" style="color: white;  text-shadow: 2px 2px 2px rgba(0,0,0,0.65); animation-delay: 0.7s" >O primeiro sashê de sal rosa do Brasil</h2>
                                <h5 class="animated fadeIn"  style="color: white; text-shadow: 2px 2px 2px rgba(0,0,0,0.65); animation-delay: 0.7s "  >
                                     Além de salgar a saladinha no seu restaurante preferido, você pode levar para casa e dosar melhor a sua utilização diária de sal. Além disso, você pode levar os nossos sachês para onde for. Podendo utilizar os sachês de sal rosa na escola, na faculdade, no trabalho ou onde mais quiser.
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>            
        </div>
    </div>
    <div class="container-fluid  div-two text-center align-items-center  h-100 ">
        <div class="row  h-100  align-items-center">
            <div class="col-md-12">
                <h3>Nossos parceiros</h3>
            </div>
            <div class="col-md-12">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_toque.png')}}">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_chinezinho_mini.png')}}">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_hippo_mini.png')}}">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_avallon_mini.png')}}">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_vitalis_mini.png')}}">
                <img class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo_animalis_mini.png')}}">
                <img style="" class="img-fluid parceiro"src="{{URL('public/img/parceiros/logo-jumbai.png')}}">
            </div>
            <div class="col-md-6 offset-md-3">
                <h5 >
                    A GEP Representação e Distribuição trabalha em parceria com grandes empresas, que estão a anos no mercado.
                    Marcas confiaveis, com produtos de qualidade e garantia de satisfação.
                </h5>
            </div>
        </div>
    </div>    
@endsection