<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GEP Representações</title>
        <!-- FAVICON type="image/png"-->
        <link rel="icon" type="image/x-icon" href="{{url('/public/favicon.ico')}}"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        {{ HTML::style('/public/css/app.css') }}
        {{ HTML::style('public/css/icomon.css') }}
        {{ HTML::style('public/css/animate.css') }}
        <!-- Js -->
        {{ HTML::script('/..js/mascaras.js') }}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </head>
    <body>
        <header>
            <nav style="" class="navbar navbar-expand-lg fixed-top navbar-light bg-light fifty shadow-sm">
                    <a class="navbar-brand" href="#">
                        <img src="{{URL('public/img/gep-logo-menu.png')}}" class="logo-menu">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{URL('/')}}">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{URL('catalogo')}}">Catálogo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModalCenter">Contato</a>
                            </li>                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{URL('quem-somos')}}">Quem Somos</a>
                            </li>
                            <!-- Dropdown
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            -->
                        </ul>
                    </div>
                </nav>
            </header>
            <section >
                @yield('content')
            </section>
            <footer class="">
                <div class="footer">
                    <div class="container">                    
                        <div class="row">
                            <div class="col-md-4 offset-md-3">
                                <form id="contato">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="nome" placeholder="Seu nome">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="email" placeholder="Seu e-mail">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="menssagem" placeholder="Sua menssagem"></textarea>
                                    </div>
                                    <button class="btn btn-app-footer">Enviar</button>
                                </form>
                            </div>
                            <div class="col-md-3">
                                <li><a href="{{URL('/')}}">Home</a></li>
                                <li><a href="{{URL('catalogo')}}">Catálogo</a></li>
                                <li><a href="{{URL('/#contato')}}">Contato</a></li>                              
                                <li><a href="{{URL('quem-somos')}}">Quem Somos</a></li>
                                <li>
                                    <a target="blanck" href="https://facebook.com/geprepresentacoes">
                                        <span class="icon-facebook2"></span>
                                        <span class="mls">/geprepresentacoes</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL('quem-somos')}}">
                                        <span class="icon-instagram"></span>
                                        <span class="mls">/geprepresentacoes</span>
                                    </a>
                                </li>
                            </div>  
                        </div>                    
                    </div>
                </div>
                <div class="container-fluid footer2">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{URL('public/img/tcwebdev.png')}}" class="footer-tcwebdev">
                        </div>
                    </div>
                </div>                                    
            </footer>
    </body>
</html>